function deepEqual(first, second) {
  let Akey = Object.keys(first);
  let Bkey = Object.keys(second);
  if (first === second){
    return true;
  } 
  if (first == null || typeof first != "object" ||second == null || typeof second != "object"){
    return false;
  } 
  if (Akey.length != Bkey.length){
    return false;
  } 

  for (let key of Akey) {
    if (!Bkey.includes(key) || !deepEqual(first[key], second[key])){
      return false;
    } 
  }
  return true;
}

let obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true