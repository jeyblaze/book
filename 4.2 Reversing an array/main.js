function reverseArray(startarr) {
  let arr = [];
  for (let i = startarr.length - 1; i >= 0; i--) {
    arr.push(startarr[i]);
  }
  return arr;
}

function reverseArrayInPlace(startarr) {
  let arr = [];
  for (let i = startarr.length - 1; i >= 0; i--) {
    arr.push(startarr[i]);
    startarr[i] = arr[i];
  }
  return startarr;
}

console.log(reverseArray(["A", "B", "C"]));
// → ["C", "B", "A"];
let arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);
// → [5, 4, 3, 2, 1]