function loop(loopanzahl, test, sprunganzahl, console) {
    for (let value = loopanzahl; test(value); value = sprunganzahl(value)) {
        console(value);
    }
  }
  
  loop(3, n => n > 0, n => n - 1, console.log);
  // → 3
  // → 2
  // → 1