function every(arr, test) {
    for (let element of arr) {
      if (!test(element)) return false;
    }
    return true;
  }
  
  function everysecond(arr, test) {
    return !arr.some(element => !test(element));
  }
  
  console.log(every([1, 3, 5], n => n < 10));
  // → true
  console.log(every([2, 4, 16], n => n < 10));
  // → false
  console.log(every([], n => n < 10));
  // → true